from LoreleiLib.Objects import Item, Living, Entity
from LoreleiLib.Character import Character
from enum import Enum


class MapDirection(Enum):

    North = 1
    East = 2
    South = 3
    West = 4


class MapExitDescriptions():

    def __init__(self, northId=None, eastId=None, southId=None, westId=None, northDescription=None, eastDescription=None, southDescription=None, westDescription=None):
        self.exits = {MapDirection.North: northId, MapDirection.East: eastId,
                       MapDirection.South: southId, MapDirection.West: westId}
        self.descriptions = {MapDirection.North: northDescription, MapDirection.East: eastDescription,
                             MapDirection.South: southDescription, MapDirection.West: westDescription}


class MapShape(Enum):

    Square = 1
    Circle = 2


class MapNode(object):

    def __init__(self, uuid, x, y, exits, z=0, shape=MapShape.Square, radius=1):
        self.uuid = uuid
        self.x = x
        self.y = y
        self.z = z
        self.shape = shape
        self.radius = radius
        self.exits = exits


class MapRegion(object):

    def __init__(self, uuid, name, description, creatures=None, materials=None):
        self.uuid = uuid
        self.name = name
        self.description = description
        self.creatures = creatures
        self.materials = materials
        self.rooms = []
        self.mapData = None

    def makeMapData(self):
        # data will be found by [z][x][y]
        self.mapData = MapData(self)

    def addRoom(self, room):
        # type: (MapTile) -> None
        self.rooms.append(room)


class MapData(object):

    def __init__(self, region):
        # type: (MapRegion) -> None
        self.floors = {} # {z: {x: {y: room}}
        for room in region.rooms:  # type: MapTile
            if not self.floors.has_key(room.z):
                self.floors[room.z] = {}
            if not self.floors[room.z].has_key(room.x):
                self.floors[room.z][room.x] = {}

            self.floors[room.z][room.x][room.y] = room.node

    def getFloor(self, z):
        return self.floors[z]


class MapTile(object):

    def __init__(self, uuid, x, y, mapRegion, description, exitDetails, z=0, shape=MapShape.Square, mapRadius=1):
        # type: (uuid, int, int, MapRegion, str, MapExitDescriptions) -> None
        self.uuid = uuid
        self.x = x # Where the tile is in its region
        self.y = y # Where the tile is in its region
        self.z = z # Where the tile is vertically
        self.region = mapRegion # What map region the tile is in
        self.description = description # Description to send the player when they enter
        self.exitDetails = exitDetails # Exit descriptions and rooms
        self.creatures = [] # List of Living Entities in the room
        self.players = [] # List of Players in the room
        self.items = [] # List of ItemDrops in the room
        self.node = MapNode(uuid, x, y, [exitDetails.exits[MapDirection.North] is not None,
                                         exitDetails.exits[MapDirection.East] is not None,
                                         exitDetails.exits[MapDirection.South] is not None,
                                         exitDetails.exits[MapDirection.West] is not None],
                            z, shape, mapRadius)

        if mapRegion is not None:
            mapRegion.addRoom(self)

    def addEntity(self, entity):
        # type: (Entity) -> None
        if isinstance(entity, Living):
            if isinstance(entity, Character):
                self.add_player(entity)
            else:
                self.creatures.append(entity)
        if isinstance(entity, Item):
            self.items.append(entity)

    def removeEntity(self, entity, killed):
        # type: (Entity, bool) -> None
        if isinstance(entity, Living):
            if isinstance(entity, Character):
                self.remove_player(entity)
            else:
                self.remove_creature(entity, killed)
        if isinstance(entity, Item):
            self.items.append(entity)

    def add_player(self, player):
        # type: (Character) -> None
        self.players.append(player)
        self.modify_player(player)

    def remove_player(self, player):
        # type: (Character) -> None
        self.players.remove(player)

    def add_creature(self, living):
        # type: (Living) -> None
        self.creatures.append(living)

    def remove_creature(self, living, killed):
        # type: (Living, bool) -> None
        self.creatures.remove(living)

    def modify_player(self, player):
        # type: (Character) -> None
        pass # No modifications in a normal tile


class TrapTile(MapTile):

    def __init__(self, x, y, mapRegion, description, exitDetails, damage, trap_name):
        # type: (int, int, str, int) -> None
        self.damage = damage
        self.trap_name = trap_name

        super(MapTile, self).__init__(x, y, mapRegion, description, exitDetails)

    def modify_player(self, player):
        # type: (Character) -> None
        player.damage(self.damage, None)


# Example implementation of a trap room that has a specific trap name
class SpikesTrap(TrapTile):
    def __init__(self, x, y, description, damage):
        # type: (int, int, str, int) -> None
        super(TrapTile, self).__init__(x, y, description, damage, "Spikes")