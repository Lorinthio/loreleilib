from LoreleiLib.Packets.Common import Packet


class CharacterPacket(Packet): pass


class CharacterDetailsPacket(CharacterPacket):

    def __init__(self, character):
        self.character = character