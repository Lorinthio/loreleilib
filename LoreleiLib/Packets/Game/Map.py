from LoreleiLib.Packets.Common import Packet
from LoreleiLib.Map import MapRegion, MapData


class MapPacket(Packet): pass


class RegionMapPacket(MapPacket):

    def __init__(self, region):
        # type: (MapRegion) -> None
        self.mapData = region.mapData


class PlayerPositionPacket(MapPacket):

    def __init__(self, uuid):
        self.uuid = uuid